import Vue from 'vue'
import VueRouter from 'vue-router'
import FormLogin from './components/FormLogin.vue'
import StarterPage from './components/StarterPage.vue'
import Home from './components/Home.vue'

import CriarChamado from './components/CriarChamado.vue'
import ListarChamados from './components/ListarChamados.vue'
import Tecnicos from './components/Tecnicos.vue'
import Componentes from './components/Componentes.vue'
import Produtos from './components/Produtos.vue'
import Clientes from './components/Clientes.vue'

import homeCliente from './components/homeCliente.vue'
import homeTecnicos from './components/homeTecnicos.vue'
import Graficos from './components/Graficos.vue'
import clienteedit from './components/clienteEdit.vue'
import tecedit from './components/tecEdit.vue'
import userInative from './components/userInative.vue'

Vue.use(VueRouter)

export default new VueRouter({
    mode: 'history',
    routes: [
        { path: '/', name: '/', component: StarterPage },
        { path: '/Home', name: 'Home', component: Home },

        { path: '/login', component: FormLogin },

        { path: '/CriarChamado', name: 'CriarChamado', component: CriarChamado },
        { path: '/ListarChamados', name: 'ListarChamados', component: ListarChamados },
        { path: '/Tecnicos', component: Tecnicos },
        { path: '/Componentes', component: Componentes },
        { path: '/Produtos', component: Produtos },
        { path: '/Clientes', component: Clientes },
        { path: '/homeCliente', component: homeCliente },
        { path: '/homeTecnicos', component: homeTecnicos },
        { path: '/Graficos', component: Graficos },
        { path: '/clienteedit', component: clienteedit },
        { path: '/tecedit', component: tecedit },
        { path: '/userinative', component: userInative },

    ]

})