from sqlalchemy.orm import defaultload
from banco import db
import hashlib
from config import config
from datetime import datetime


class Usuario(db.Model):
    __tablename__ = 'usuarios'
    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    nome = db.Column(db.String(60), nullable=False)
    email = db.Column(db.String(120), nullable=False, unique=True)
    senha = db.Column(db.String(32), nullable=False)
    access = db.Column(db.Integer, nullable=False)
    flag = db.Column(db.Integer, nullable=False, default=1)
    created = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    cpf = db.Column(db.String(32), nullable=False)


    @staticmethod
    def from_json(json_usuarios):
        nome = json_usuarios.get('nome')
        email = json_usuarios.get('email')
        access = json_usuarios.get('access')
        flag = json_usuarios.get('flag')
        cpf = json_usuarios.get('cpf')
        senha = json_usuarios.get('senha') + config.SALT
        senha_md5 = hashlib.md5(senha.encode()).hexdigest()
        return Usuario(nome=nome, email=email, senha=senha_md5, access=access, flag=flag, cpf = cpf)

    def to_json(self):
        json_usuarios = {
            'id': self.id,
            'nome': self.nome,
            'email': self.email,
            'access': self.access,
            'flag': self.flag,
            'created' : self.created,
            'cpf': self.cpf
        }
        return json_usuarios
    
    def to_json_tecnico(self, json_usuarios):
        json_usuarios = {
            'user_id': self.id,
            'telefone' : json_usuarios.get('telefone'),
        }
        return json_usuarios

    def to_json_cliente(self, json_usuarios):
        json_usuarios = {
            'user_id': self.id,
            'telefone' : json_usuarios.get('telefone'),
            'endereco': json_usuarios.get('endereco'),
            'valor': json_usuarios.get('valor'),
        }
        return json_usuarios