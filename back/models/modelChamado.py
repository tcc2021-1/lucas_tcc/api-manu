from sqlalchemy.orm import defaultload, defer
from banco import db
from datetime import datetime
import math

class Chamado(db.Model):
    __tablename__ = 'chamados'
    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    defRelatado = db.Column(db.String(500), nullable=False)
    defApresentado = db.Column(db.String(500), nullable=True)
    valor = db.Column(db.String(5), nullable=True)
    status = db.Column(db.String(5), nullable=False)

    #previsão de passar orçamento
    dataPrev = db.Column(db.String(10), nullable=True)
    #retirada do equipamento
    dataDelivery= db.Column(db.DateTime, nullable=True)
    #data que expira a garantia
    dataWaranty = db.Column(db.DateTime, nullable=True)
    #meses que terá de garantia
    periodWaranty = db.Column(db.String(5), nullable=True)

    componentsJson= db.Column(db.JSON, nullable=True)
    dataCria = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

    tecnico_id = db.Column(db.Integer, db.ForeignKey(
        'tecnicos.id'), nullable=True)
    cliente_id = db.Column(db.Integer, db.ForeignKey(
        'clientes.id'), nullable=False)
    produto_id = db.Column(db.Integer, db.ForeignKey(
        'produtos.id'), nullable=False)
 
    tecnicos = db.relationship('Tecnico') 
    clientes = db.relationship('Cliente')
    produtos = db.relationship('Produto')
    
#envia
    def to_json(self):
        json_chamados = {
            'id': self.id,
            'defRelatado': self.defRelatado,
            'defApresentado': self.defApresentado,
            'valor': self.valor,
            'status': self.status,

            'dataPrev': self.dataPrev,
            'dataDelivery': self.dataDelivery,
            'dataWaranty': self.dataWaranty,
            'periodWaranty': self.periodWaranty,

            'tecnico_id': self.tecnico_id,
            'cliente_id': self.cliente_id,
            'componentsJson': self.componentsJson,
            'produto_id': self.produto_id,
            'dataCria': self.dataCria,
        }
        return json_chamados
#recebe
    @staticmethod
    def from_json(json_chamados):
        defRelatado = json_chamados.get('defRelatado')
        defApresentado = json_chamados.get('defApresentado')
        valor = json_chamados.get('valor')
        status = json_chamados.get('status')
        componentsJson= json_chamados.get('componentsJson')

        dataPrev= json_chamados.get('dataPrev')
        dataDelivery= json_chamados.get('dataDelivery')
        periodWaranty= json_chamados.get('periodWaranty')
        dataWaranty= json_chamados.get('dataWaranty')

        tecnico_id = json_chamados.get('tecnico_id')
        cliente_id = json_chamados.get('cliente_id')
        produto_id = json_chamados.get('produto_id')

        return Chamado(valor=valor, status=status, dataPrev=dataPrev, dataWaranty=dataWaranty, periodWaranty=periodWaranty,dataDelivery=dataDelivery, defApresentado =defApresentado, defRelatado = defRelatado, tecnico_id=tecnico_id, cliente_id=cliente_id,produto_id=produto_id,componentsJson=componentsJson )
