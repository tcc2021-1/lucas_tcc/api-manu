from banco import db


class Tecnico(db.Model):
    __tablename__ = 'tecnicos'
    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    telefone = db.Column(db.String(20), nullable=True)

    user_id = db.Column(db.Integer, db.ForeignKey(
        'usuarios.id'), nullable=False)

    user = db.relationship('Usuario')

    def to_json(self):
        json_tecnicos = {
            'id': self.id,
            'telefone': self.telefone,
            'nome': self.user.nome,
            'email': self.user.email,
            'user_id': self.user_id,
            'cpf': self.user.cpf
        }
        return json_tecnicos

    @staticmethod
    def from_json(json_tecnicos):
        user_id = json_tecnicos.get('user_id')
        telefone = json_tecnicos.get('telefone')
        return Tecnico(user_id=user_id, telefone=telefone)
