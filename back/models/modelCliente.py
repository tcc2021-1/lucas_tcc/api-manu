from banco import db

class Cliente(db.Model):
    __tablename__ = 'clientes'
    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    telefone = db.Column(db.String(20), nullable=False)
    endereco = db.Column(db.String(500), nullable=False)
    valor = db.Column(db.Float(9.4), nullable=True)

    user_id = db.Column(db.Integer, db.ForeignKey(
        'usuarios.id'), nullable=False)

    user = db.relationship('Usuario')


    def to_json(self):
        json_clientes = {
            'id': self.id,
            'telefone': self.telefone,
            'nome': self.user.nome,
            'email': self.user.email,
            'user_id': self.user_id,
            'endereco': self.endereco,
            'valor': self.valor,
            'cpf': self.user.cpf
        }
        return json_clientes

    @staticmethod
    def from_json(json_clientes):
        user_id = json_clientes.get('user_id')
        telefone = json_clientes.get('telefone')
        endereco = json_clientes.get('endereco')
        valor = json_clientes.get('valor')
        return Cliente(telefone=telefone, endereco=endereco, valor= valor, user_id = user_id)
