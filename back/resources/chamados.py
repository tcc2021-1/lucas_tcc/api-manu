from flask import Blueprint, jsonify, request
from banco import db
from flask_cors import CORS, cross_origin
from models.modelChamado import Chamado
from models.modelUsuario import Usuario
from models.modelCliente import Cliente
from models.modelProduto import Produto
from datetime import datetime
import math


from flask_jwt_extended import jwt_required, get_jwt

chamados = Blueprint('chamados', __name__)

@jwt_required()
def user_id():
    claims = get_jwt()
    user_id=claims["user_id"]
    return user_id

@chamados.route('/chamados/<int:id>')
# @jwt_required
@cross_origin()
def getByID(id):
    chamado = Chamado.query.get_or_404(id)
    return jsonify(chamado.to_json()),200

@chamados.route('/chamados')
# @jwt_required()
@cross_origin()
def listagemChamado():
    chamados = Chamado.query.order_by( Chamado.status, Chamado.id).all()
    return jsonify([chamado.to_json() for chamado in chamados])

@chamados.route('/chamadosmonth/<int:id>')
# @jwt_required()
@cross_origin()
def listagemChamadoData(id):
    if id==1:
        id=12
    startDate = datetime.utcnow().replace(month = id-1)
    endDate = datetime.utcnow().replace(month= id)

    chamados = Chamado.query.order_by(Chamado.id).filter(Chamado.dataCria.between (startDate,endDate))
    return jsonify([chamado.to_json() for chamado in chamados])
    

@chamados.route('/homeClientes')
@jwt_required()
@cross_origin()
def chamadoClientes():
    claims = get_jwt()
    user_id=claims["user_id"]
    chamados = Chamado.query.order_by(Chamado.id).filter_by(cliente_id = user_id).all()
    return jsonify([chamado.to_json() for chamado in chamados])


@chamados.route('/chamados/cpf/<string:cpfid>')
# @jwt_required()
@cross_origin()
def getByCpf(cpfid):

    clientes = Cliente.query.order_by(Cliente.id).join(Usuario).filter(
    Usuario.cpf.like(f'%{cpfid}%')).first()

    chamados = Chamado.query.order_by(Chamado.id).filter(
    Chamado.cliente_id ==clientes.user_id).all()

    #bolean testa se chamados não existe
    if chamados:
        response = [chamado.to_json() for chamado in chamados]
    else:
        response={'erro': 'nenhum registro anterior encontrado'}
    # converte a lista de filmes para o formato JSON (list comprehensions)
    return jsonify(response)
    
@chamados.route('/chamados/product/<string:cpfid>')
# @jwt_required()
@cross_origin()
def getByProduct(cpfid):
    chamados = Chamado.query.order_by(Chamado.id).filter(
    Chamado.produto_id.like(f'%{cpfid}%')).all()
    #bolean testa se chamados não existe
    if chamados:
        response = [chamado.to_json() for chamado in chamados]
    else:
        response={'erro': 'nenhum registro anterior encontrado'}
    # converte a lista de filmes para o formato JSON (list comprehensions)
    return jsonify(response)

@chamados.route('/chamados/order/<string:cpfid>')
# @jwt_required()
@cross_origin()
def getByOrderID(cpfid):
    chamados = Chamado.query.order_by(Chamado.id).filter(
    Chamado.id.like(f'%{cpfid}%')).all()
    #bolean testa se chamados não existe
    if chamados:
        response = [chamado.to_json() for chamado in chamados]
    else:
        response={'erro': 'nenhum registro anterior encontrado'}
    # converte a lista de filmes para o formato JSON (list comprehensions)
    return jsonify(response)



# -------------------------------

@chamados.route('/chamados', methods=['POST'])
# Quando colocar o login descomentar essa linha
# @jwt_required
@cross_origin()
def cadastroChamado():
    chamado = Chamado.from_json(request.json)
    db.session.add(chamado)
    db.session.commit()
    return jsonify(chamado.to_json()), 201

@chamados.route('/homeTecnico/<int:id>', methods=['PUT'])
@jwt_required()
@cross_origin()
def orcar(id):
    claims = get_jwt()
    user_id=claims["user_id"]
    
    chamado = Chamado.query.get_or_404(id)

    chamado.defApresentado = request.json['defApresentado']
    chamado.valor = request.json['valor']
    chamado.status = request.json['status']
    chamado.componentsJson = request.json['componentsJson']
    chamado.tecnico_id = user_id


    db.session.add(chamado)
    db.session.commit()
    return jsonify(chamado.to_json()),200


@chamados.route('/homeCliente/<int:id>', methods=['PUT'])
# @jwt_required
@cross_origin()
def aprova(id):
    chamado = Chamado.query.get_or_404(id)

    chamado.status = request.json['status']
    
    db.session.add(chamado)
    db.session.commit()
    return jsonify(chamado.to_json()),200

@chamados.route('/chamados/<int:id>', methods=['PUT'])
# @jwt_required
@cross_origin()
def alterar(id):
    chamado = Chamado.query.get_or_404(id)

    chamado.defRelatado= request.json['defRelatado']
    chamado.valor = request.json['valor']
    chamado.status = request.json['status']
    chamado.dataPrev = request.json['dataPrev']
    chamado.cliente_id = request.json['cliente_id']
    chamado.produto_id = request.json['produto_id']

    db.session.add(chamado)
    db.session.commit()
    return jsonify(chamado.to_json()),200

@chamados.route('/chamados/end/<int:id>', methods=['PUT'])
# @jwt_required
@cross_origin()
def encerrar(id):
    chamado = Chamado.query.get_or_404(id)

    chamado.valor = request.json['valor']
    chamado.status = request.json['status']
    chamado.dataDelivery = datetime.utcnow() 
    chamado.periodWaranty = request.json['periodWaranty']
    if chamado.periodWaranty:
            varPeriodWarranty = int(chamado.periodWaranty)
            plusYear=0
            if varPeriodWarranty>12:
                plusYear= math.floor(varPeriodWarranty/12)
                varPeriodWarranty=varPeriodWarranty-(plusYear*12)
            atualDate = datetime.utcnow() 
            if (atualDate.month+varPeriodWarranty)> 12:
                warranty = datetime.utcnow().replace(month= atualDate.month+varPeriodWarranty-12, year=atualDate.year+1+plusYear)
            else:
                warranty = datetime.utcnow().replace(month= atualDate.month+varPeriodWarranty, year=atualDate.year+plusYear)
            dataWaranty = warranty
    else:
        dataWaranty=  None
    chamado.dataWaranty = dataWaranty

    db.session.add(chamado)
    db.session.commit()
    
    produto = Produto.query.get_or_404( request.json['produto_id'])
    produto.dataGarantia = dataWaranty
    db.session.add(produto)
    db.session.commit() 

    return jsonify(chamado.to_json()),200




@chamados.route('/chamados/<int:id>', methods=['DELETE'])
# Quando colocar o login descomentar essa linha
# @jwt_required
@cross_origin()
def excluir(id):
    Chamado.query.filter_by(id= id).delete()
    db.session.commit()
    return jsonify({'id':id,'message': 'Chamado excluído com sucesso'}), 200
