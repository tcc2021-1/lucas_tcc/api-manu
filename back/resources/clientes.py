from banco import db
from flask import Blueprint,jsonify, request
from config import config
import hashlib
from flask_jwt_extended import create_access_token, jwt_required, get_jwt
from blocklist import blocklist
from models.modelCliente import Cliente
from models.modelUsuario import Usuario
from flask_cors import CORS, cross_origin

clientes = Blueprint('clientes', __name__)

@clientes.route('/clientes')
def listagemClientes():
    clientes = Cliente.query.order_by(Cliente.id).all()
    return jsonify([cliente.to_json() for cliente in clientes])

@clientes.route('/cliente/<int:id>')
def getByID(id):
    cliente = Cliente.query.get_or_404(id)
    return jsonify(cliente.to_json()),200

@clientes.route('/clienteedit')
@jwt_required()
@cross_origin()
def clienteEdit():
    claims = get_jwt()
    user_id=claims["user_id"]
    print("user",user_id)
    cliente = Cliente.query.order_by(Cliente.id).filter_by(user_id = user_id).first()
    return jsonify(cliente.to_json()),200

@clientes.route('/clientes/<string:cpf>')
def getBycpf(cpf):
    clientes = Cliente.query.order_by(Cliente.id).join(Usuario).filter(
    Usuario.cpf.like(f'%{cpf}%')).all()
    #bolean testa se clientes não existe
    if clientes:
        response = [cliente.to_json() for cliente in clientes]
    else:
        response={'erro': 'not found'}
    # converte a lista de filmes para o formato JSON (list comprehensions)
    return jsonify(response)
    



@clientes.route('/loginCliente', methods=['POST'])
def loginCliente():
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400

    email = request.json.get('email', None)
    senha = request.json.get('senha', None)
    if not email:
        return jsonify({"msg": "Missing email parameter"}), 400
    if not senha:
        return jsonify({"msg": "Missing senha parameter"}), 400

    senha += config.SALT
    senha_md5 = hashlib.md5(senha.encode()).hexdigest()

    cliente = Cliente.query \
        .filter(Cliente.email == email) \
        .filter(Cliente.senha == senha_md5) \
        .first()

    if cliente:
        # Identity can be any data that is json serializable
        access_token = create_access_token(identity=email)
        return jsonify({"user": cliente.id, "access_token": access_token}), 200
    else:
        return jsonify({"user": None, "access_token": None}), 200


@clientes.route('/logout')
@jwt_required()
def logout():
    jti = get_jwt()['jti']
    blocklist.add(jti)
    return jsonify({"msg": "Successfully logged out"}), 200
