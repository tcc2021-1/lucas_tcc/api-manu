from flask import Flask
from flask_cors import CORS, cross_origin
from config import config
from banco import db
from blocklist import blocklist



from resources.usuarios import usuarios
from resources.chamados import chamados
from resources.produtos import produtos
from resources.clientes import clientes
from resources.tecnicos import tecnicos
from resources.componentes import componentes

from flask_jwt_extended import JWTManager

from flask_jwt_extended import create_access_token
from flask_jwt_extended import get_jwt
from flask_jwt_extended import jwt_required


app = Flask(__name__)

#classe config da onde vem as configurações
app.config.from_object(config)

db.init_app(app)

jwt = JWTManager(app)

CORS(app)

app.register_blueprint(usuarios)
app.register_blueprint(chamados)
app.register_blueprint(clientes)
app.register_blueprint(tecnicos)
app.register_blueprint(produtos)
app.register_blueprint(componentes)


# with app.app_context():
#     api = Api(app)
#     db.init_app(app)


# @jwt.token_in_blocklist_loader
# def check_if_token_in_blocklist(decrypted_token):
#     jti = decrypted_token['jti']
#     return jti in blocklist

@jwt.token_in_blocklist_loader
def check_if_token_is_revoked(jwt_header, jwt_payload):
    jti = jwt_payload["jti"]
    return jti in blocklist

@app.route('/')
def raiz():
    db.create_all()
    return '<h2>Banco de dados Assist Criado</h2>'


if __name__ == '__main__':
    app.run(debug=True)
